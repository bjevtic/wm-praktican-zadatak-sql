﻿using System.Web;
using System.Web.Mvc;

namespace Praktican_zadatak_SQL___Bosko_Jevtic
{
    public class FilterConfig
    {
        public static void RegisterGlobalFilters(GlobalFilterCollection filters)
        {
            filters.Add(new HandleErrorAttribute());
        }
    }
}
