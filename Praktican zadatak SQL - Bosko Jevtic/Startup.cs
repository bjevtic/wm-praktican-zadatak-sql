﻿using Microsoft.Owin;
using Owin;

[assembly: OwinStartupAttribute(typeof(Praktican_zadatak_SQL___Bosko_Jevtic.Startup))]
namespace Praktican_zadatak_SQL___Bosko_Jevtic
{
    public partial class Startup
    {
        public void Configuration(IAppBuilder app)
        {
            ConfigureAuth(app);
        }
    }
}
