﻿using Praktican_zadatak_SQL___Bosko_Jevtic.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;


namespace Praktican_zadatak_SQL___Bosko_Jevtic.Controllers
{
    public class ProizvodController : Controller
    {
        // GET: Proizvod
        public ActionResult Index()
        {
            PomocnoDBEntities db = new PomocnoDBEntities();
            List<Proizvod> proizvodList = db.Proizvods.ToList();
            ProizvodViewModel proizvodVM = new ProizvodViewModel();
            List<ProizvodViewModel> proizvodVMList = proizvodList.Select(x => new ProizvodViewModel {
                Id = x.Id,
                Naziv = x.Naziv,
                Opis = x.Opis,
                Kategorija = x.Kategorija,
                Proizvodjac =x.Proizvodjac,
                Dobavljac =x.Dobavljac,
                Cena =x.Cena }).ToList();
            return View(proizvodVMList);
        }

        public ActionResult CreateRec(ProizvodViewModel model)
        {
            if (model.Naziv != null && model.Opis != null && model.Kategorija != null && model.Proizvodjac != null && model.Dobavljac != null)
            {
                PomocnoDBEntities db = new PomocnoDBEntities();
                Proizvod element = new Proizvod();
                element.Naziv = model.Naziv;
                element.Opis = model.Opis;
                element.Kategorija = model.Kategorija;
                element.Proizvodjac = model.Proizvodjac;
                element.Dobavljac = model.Dobavljac;
                element.Cena = model.Cena;

                db.Proizvods.Add(element);
                db.SaveChanges();
            }
            return View();
        }

        public ActionResult DeleteRec(ProizvodViewModel model)
        {
            if (model.Id != 0)
            {
                PomocnoDBEntities db = new PomocnoDBEntities();
                Proizvod element = new Proizvod();
                element = db.Proizvods.Find(model.Id);
                if (element != null)
                {
                    db.Proizvods.Remove(element);
                    db.SaveChanges();
                }
            }
            return View();
        }

        public ActionResult UpdateRec(ProizvodViewModel model)
        {
            if (model.Id != 0)
            {
                PomocnoDBEntities db = new PomocnoDBEntities();
                Proizvod element = new Proizvod();
                element = db.Proizvods.Find(model.Id);
                if (element != null)
                {
                    db.Proizvods.Remove(element);

                    Proizvod elementNew = new Proizvod();
                    elementNew.Naziv = model.Naziv;
                    elementNew.Opis = model.Opis;
                    elementNew.Kategorija = model.Kategorija;
                    elementNew.Proizvodjac = model.Proizvodjac;
                    elementNew.Dobavljac = model.Dobavljac;
                    elementNew.Cena = model.Cena;
                    db.Proizvods.Add(elementNew);

                    db.SaveChanges();
                }
            }
            return View();
        }


    }
}